import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {FormsModule} from '@angular/forms'
import {AppComponent} from './app.component';
import {StudentService} from "./student/student.service";
import {HttpModule} from "@angular/http";

import {AccordionModule, AlertModule, ButtonsModule, ModalModule} from 'ngx-bootstrap';
import {AddStudentComponent} from './add-student/add-student.component';
import {ViewStudentComponent} from './view-student/view-student.component';
import {DeleteStudentComponent} from './delete-student/delete-student.component';
import {UpdateStudentComponent} from './update-student/update-student.component';
import { ListStudentsComponent } from './list-students/list-students.component';


const appRoutes: Routes = [
  /*{path: 'crisis-center', component: CrisisListComponent},
  {path: 'hero/:id', component: HeroDetailComponent},
  {
    path: 'heroes',
    component: HeroListComponent,
    data: {title: 'Heroes List'}
  },
  {
    path: '',
    redirectTo: '/heroes',
    pathMatch: 'full'
  },
  {path: '**', component: PageNotFoundComponent}*/
];

@NgModule({
  declarations: [
    AppComponent,
    AddStudentComponent,
    ViewStudentComponent,
    DeleteStudentComponent,
    UpdateStudentComponent,
    ListStudentsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ModalModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    AccordionModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes, {enableTracing: true}
    )
  ],
  providers: [StudentService, HttpModule],
  bootstrap: [AppComponent]
})
export class AppModule {
}
