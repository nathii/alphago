import {Injectable} from '@angular/core';
import {Student} from './student';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import '../../../node_modules/rxjs/Observable';
import {Observable} from 'rxjs';


@Injectable()
export class StudentService {

  private headers = new Headers({'Content-Type': 'application/json'});
  private options = new RequestOptions({headers: this.headers});

  private baseUrl = "http://localhost:8080/students";
  private getByID = "";
  private add = "";
  private deleteByID = "";
  private update = "";

  constructor(private http: Http) {

  }

  getAllStudents(): Observable<Student[]> {

    return this.http.get(`${this.baseUrl}`, this.options)
      .map((res: Response) => <Student[]>res.json())
      .catch(err => this.handleError(err));

  }


  getStudentByID(ID: String): Observable<Student> {
    this.getByID = "getStudentByID";

    return this.http.get(`${this.baseUrl.concat(this.getByID)}/${ID}`, this.options)
      .map(response => <Student>response.json())
      .catch(err => this.handleError(err));

  }

  addStudent(student: Student): Observable<Response> {
    this.add = "addStudent";

    return this.http.post(`${this.baseUrl.concat(this.add)}`, student, this.options)
      .map(response => response)
      .catch(err => this.handleError(err))
  }

  deleteStudentByID(ID: String): Observable<Response> {
    this.deleteByID = "deleteStudentByID";

    return this.http.delete(`${this.baseUrl.concat(this.deleteByID)}/${ID}`, this.options)
      .map(response => response)
      .catch(err => this.handleError(err))

  }

  updateStudent(student: Student, oldID: string): Observable<Response> {
    this.update = "updateStudent";
    student.student_num += ("#" + oldID);

    return this.http.post(`${this.baseUrl.concat(this.update)}`, student, this.options)
      .map(response => response)
      .catch(err => this.handleError(err))
  }


  private handleError(error: any): Promise<Array<any>> {
    // console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
