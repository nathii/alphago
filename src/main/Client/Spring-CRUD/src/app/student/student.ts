export class Student {
  public student_num: string;
  public name: string;
  public course: string;
  public read: string;

  constructor(_studentID: string, _name: string, _course: string) {
    this.student_num = _studentID;
    this.name = _name;
    this.course = _course;
    this.read = "";
  }
}
