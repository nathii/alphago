import {Component, OnInit} from '@angular/core';
import {StudentService} from "./student/student.service";
import {Student} from "./student/student";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  studentArray: any;
  studentOBJ: Student;
  newStudent: Student;
  student: string;

  modalState: string;
  lastRead: string;

  mode: boolean;
  showAddModal: boolean;
  formError: boolean;
  addSuccess: boolean;
  deleteSuccess: boolean;
  deleteModal: boolean;
  confirmDelete: boolean;
  confirmEdit: boolean;
  updateSuccess: boolean;

  formName: string;
  formStudentNum: string;
  formCourse: string;

  delName: string;
  delStudentNum: string;
  delCourse: string;

  constructor(public studentService: StudentService) {

    this.mode = true;
    this.modalState = "";
    this.formError = false;
    this.confirmDelete = false;
    this.addSuccess = false;
    this.deleteSuccess = false;
    this.deleteModal = false;
    this.confirmEdit = false;
    this.updateSuccess = false;
    this.delCourse = "";
    this.lastRead = "";
    this.delName = "";
    this.delStudentNum = "";
    this.student = "";
    this.formName = "";
    this.formStudentNum = "";
    this.formCourse = "";
    this.studentArray = new Array();
    this.showAddModal = false;
    this.studentOBJ = new Student("", "", "");
  }

  ngOnInit() {

    this.getAllStudents();
  }


  /**************** Get Students functionality *****************/

  getAllStudents() {

    this.mode = true;
    this.confirmDelete = false;
    this.confirmEdit = false;
    this.studentOBJ = new Student("", "", "");

    this.studentService.getAllStudents().subscribe(
      (res) => {
        this.studentArray = res;
        console.log(this.studentArray);
      },
      (err) => {
        console.log(err);
      });
  }

  getStudentByID() {
    this.mode = false;
    this.studentService.getStudentByID(this.student).subscribe(
      (studentX) => {
        this.studentOBJ = studentX;
        console.log(studentX);
      },
      err => {
        console.debug(err);
      });

    this.lastRead = this.studentOBJ.read;
  }


  /**************** Add Student functionality *****************/

  addStudentOpenDialog() {
    console.log("Open dialog to add new student.");
    this.formName = this.formCourse = this.formStudentNum = "";
    this.modalState = "addModal";
    this.showAddModal = true;
  }

  closeModal() {
    this.showAddModal = false;
    this.addSuccess = false;
    this.formError = false;
    this.getAllStudents();
  }

  sendStudentData() {

    console.log("Test: " + this.formName + ", " + this.formStudentNum + ", " + this.formCourse);

    if (this.formName == "" || this.formCourse == "" || this.formStudentNum == "") {
      this.formError = true;
    } else {

      this.newStudent = new Student(this.formStudentNum, this.formName, this.formCourse);
      this.studentService.addStudent(this.newStudent).subscribe(
        (res) => {

          if (res.statusText == "OK") {
            this.getAllStudents();
          }
        },
        (err) => {
          console.log(err);
        });

      this.showAddModal = false;
      this.formError = false;
      this.addSuccess = true;
      this.modalState = "";
    }
  }

  dismissAlertAdd() {

    this.addSuccess = false;
  }


  /**************** Delete Student functionality *****************/
  deleteStudentByID(ID: string, _name: string, _course: string) {

    console.log("Delete student: " + ID);

    this.delName = _name;
    this.delStudentNum = ID;
    this.delCourse = _course;

    console.log("Check: " + this.delName + ", " + this.delStudentNum + ", " + this.delCourse);

    if (this.delStudentNum.length > 1) {
      this.showAddModal = true;
      this.modalState = "deleteModal";
    }

    if (this.confirmDelete) {
      this.deleteStudent(this.delStudentNum);
    }
  }

  deleteStudent(ID: string) {

    console.log("DELETE: " + this.delStudentNum);

    if (this.delStudentNum.length > 1) {
      this.studentService.deleteStudentByID(this.delStudentNum).subscribe(
        (res) => {

          if (res.statusText == "OK") {
            this.getAllStudents();
          }
        },
        (err) => {
          console.log(err);
        }
      )

      this.showAddModal = false;
      this.modalState = "";

      this.closeModal();
      this.deleteSuccess = true;
    }
  }

  cancelDelete() {

    this.showAddModal = false;
    this.deleteSuccess = false;
    this.modalState = "";
    this.confirmDelete = false;

    this.closeModal();
  }

  delete() {
    this.confirmDelete = true;
  }

  dismissAlertDelete() {

    this.deleteSuccess = false;
  }


  /**************** Update Student functionality *****************/
  updateStudent(student: Student, oldID: string) {

    console.log("Update student: " + student.student_num);

    this.studentService.updateStudent(student, oldID).subscribe(
      (res) => {
        if (res.statusText == "OK") {
          this.getAllStudents();
        }
      },
      (err) => {
        console.log(err);
      })

    this.closeModal();
    this.updateSuccess = true;
  }

  openUpdateModal(ID: string, _name: string, _course: string) {

    this.delName = _name;
    this.delStudentNum = ID;
    this.delCourse = _course;

    this.formName = this.formCourse = this.formStudentNum = "";
    console.log("Update-check: " + this.delName + ", " + this.delStudentNum + ", " + this.delCourse);

    if (this.delStudentNum.length > 1) {
      this.showAddModal = true;
      this.modalState = "editModal";

      if (this.confirmEdit) {

      }
    }
  }

  setUpdates() {

  }

  closeUpdateAlert() {
    this.updateSuccess = false;
  }

  update() {
    this.confirmEdit = true;

    if (this.formStudentNum.length == 0) {
      this.formStudentNum = this.delStudentNum;
    }
    if (this.formName.length == 0) {
      this.formName = this.delName;
    }
    if (this.formCourse.length == 0) {
      this.formCourse = this.delCourse;
    }

    console.log("UPDATER: " + this.formStudentNum + ", " + this.formName + ", " + this.formCourse);

    this.updateStudent(new Student(this.formStudentNum, this.formName, this.formCourse), this.delStudentNum);
  }


  /**************** View Student functionality *****************/
  viewStudent(student: Student) {
    console.log("Viewing student: " + student.student_num);

    this.delName = student.name;
    this.delStudentNum = student.student_num;
    this.delCourse = student.course;
    this.lastRead = student.read;

    this.formName = this.formCourse = this.formStudentNum = "";

    this.showAddModal = true;
    this.modalState = "viewStudentModal";
  }

  private handleError(error: any): Promise<Array<any>> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

}
