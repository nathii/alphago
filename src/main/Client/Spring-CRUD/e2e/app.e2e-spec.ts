import { SpringCRUDPage } from './app.po';

describe('spring-crud App', () => {
  let page: SpringCRUDPage;

  beforeEach(() => {
    page = new SpringCRUDPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
