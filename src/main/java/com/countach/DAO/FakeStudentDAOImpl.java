package com.countach.DAO;

import com.countach.Entity.Student;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Use for testing purposes locally.
 */

@Repository
@Qualifier("Fake-Data")
public class FakeStudentDAOImpl implements StudentDAO
{
    private static Map<String, Student> students;  //StudentID maps to Student Object.
    private static ArrayList<Student> formerStudents;

    public FakeStudentDAOImpl() {}

    static
    {
        students = new HashMap<String, Student>()
        {
            {
                put("R874120", new Student("R874120", "Rego Neelo", "BCom Accounting"));
                put("T784568", new Student("T784568", "Kia Mayeni", "BEng Industrial Engineering"));
                put("W451236", new Student("W451236", "Mandisa Putin", "BSc Applied Mathematics"));
                put("Q784777", new Student("Q784777", "Jake Sulli", "BCom Law"));
                put("J123647", new Student("J123647", "Sechaba Popus", "BSc Computer Science"));
            }
        };

        formerStudents = new ArrayList<>();
    }

    @Override
    public Collection<Student> getAllStudents()
    {
        return students.values();
    }


    @Override
    public Student getStudentByID(String ID)
    {
        return (students.get(ID));
    }

    @Override
    public Boolean deleteStudentByID(int ID, String studentID)
    {
        if(students.containsKey(ID))
        {
            students.remove(ID);
            return true;
        }

        return false;
    }

    /**
     * The Student object with ID = stoo.getID() is updated with the attributes of the Student "stoo".
     * Overwritten Students are added to the "formerStudents" List (Persistence).
     * @param stoo Object to overwrite existing Student object.
     */
    @Override
    public void updateStudent(Student stoo)
    {
        if(stoo != null)
        {
            if(students.containsKey(stoo.getStudentID()))
            {
                System.out.println("Student-Updated: " + stoo.getStudentID() + ", " + stoo.getName() + ", " + stoo.getCourse());

                if(formerStudents.add(students.get(stoo.getStudentID())))
                {
                    System.out.println("Former-Students Populated: " + formerStudents.get(formerStudents.size() - 1).getStudentID() + ", " +formerStudents.get(formerStudents.size() - 1).getName() + ", " + formerStudents.get(formerStudents.size() - 1).getCourse());
                }

                students.put(stoo.getStudentID(), stoo);
            }
            else
            {
                this.addStudent(stoo);
            }
        }
        else
        {
            System.out.println("Student is null!");
        }
    }

    @Override
    public Collection<Student> getStudentsByCourse(String co)
    {
        return null;
    }

    @Override
    public Boolean addStudent(Student stoo)
    {
        if(stoo != null)
        {
            if(!students.containsKey(stoo.getStudentID()))
            {
                System.out.println("Student-Added: " + stoo.getStudentID() + ", " + stoo.getName() + ", " + stoo.getCourse());
                students.put(stoo.getStudentID(), stoo);
                return true;
            }

            System.out.println("Key already taken ..");
        }
        else
        {
            System.out.println("Student is null!");
        }

        return false;
    }

    @Override
    public int getStudentPrimaryKey(String id)
    {
        return 0;
    }
}
