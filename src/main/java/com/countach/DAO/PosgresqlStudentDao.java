package com.countach.DAO;

import com.countach.Entity.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Optional;

@Repository
public class PosgresqlStudentDao implements CrudRepository<Student, Long> {

    private static DatabaseConnectionConfig DBCC;

    static {
        DBCC = new DatabaseConnectionConfig("jdbc:postgresql://localhost:5432/postgres", "org.postgresql.Driver",
                                            "2580456NxM", "postgres");
    }

    @Override
    public <S extends Student> S save(S s) {
        return null;
    }

    @Override
    public <S extends Student> Iterable<S> saveAll(Iterable<S> iterable) {
        return null;
    }

    @Override
    public Optional<Student> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public boolean existsById(Long aLong) {
        return false;
    }

    @Override
    public Iterable<Student> findAll() {

        ArrayList<Student> students = new ArrayList<>();

        Connection connection;
        Statement statement;
        ResultSet resultSet;


        try {
            String query = "SELECT * FROM Student;";

            connection = DBCC.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                students.add(new Student(resultSet.getString("student_num"),
                        resultSet.getString("name"),
                        resultSet.getString("course")));

                students.get(students.size() - 1).setRead(this.initTimestamp());
            }

            statement.close();
            connection.commit();
            connection.close();
        }
        catch(Exception exp) {
            throw new RuntimeException("Failed to get all students!", exp);
        }

        return students;
    }

    @Override
    public Iterable<Student> findAllById(Iterable<Long> iterable) {
        return null;
    }

    @Override
    public long count() {
        return 0;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Student student) {

    }

    @Override
    public void deleteAll(Iterable<? extends Student> iterable) {

    }

    @Override
    public void deleteAll() {

    }

    private String initTimestamp(){

        String timeZone = (Integer.toString(LocalTime.now().getHour()));
        timeZone += (":" + Integer.toString(LocalTime.now().getMinute()));
        timeZone += (":" + Integer.toString(LocalTime.now().getSecond()));

        String stamp = "";
        LocalDate localDate = LocalDate.now();
        stamp += Integer.toString(localDate.getDayOfMonth());
        stamp += (" " + Month.of(localDate.getMonthValue()).name());
        stamp += (" " + Integer.toString(localDate.getYear()) + ", ");
        stamp += timeZone;

        return stamp;
    }
}
