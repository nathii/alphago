package com.countach.DAO;

import com.countach.Entity.Student;
import com.countach.Entity.StudentNotFoundException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
@Qualifier("Firestore")
public class FirestoreStudentDAO implements StudentDAO {

//    private Firestore db;

    public FirestoreStudentDAO() {
        //this.db = FirestoreClient.getFirestore();
    }

    @Override
    public Collection<Student> getAllStudents() {

        //CollectionReference collectionReference = this.db.collection("Student");

        return null;
    }

    @Override
    public Collection<Student> getStudentsByCourse(String co) {
        return null;
    }

    @Override
    public Student getStudentByID(String ID) throws StudentNotFoundException {
        return null;
    }

    @Override
    public Boolean deleteStudentByID(int ID, String studentID) {
        return null;
    }

    @Override
    public void updateStudent(Student stoo) {

    }

    @Override
    public Boolean addStudent(Student stoo) {
        return null;
    }

    @Override
    public int getStudentPrimaryKey(String id) {
        return 0;
    }
}
