package com.countach.DAO;

import com.countach.Entity.Student;
import com.countach.Entity.StudentNotFoundException;

import java.util.Collection;

/**
 * Created by countach on 2017/12/24.
 */
public interface StudentDAO
{
    Collection<Student> getAllStudents();
    Collection<Student> getStudentsByCourse(String co);
    Student getStudentByID(String ID) throws StudentNotFoundException;
    Boolean deleteStudentByID(int ID, String studentID);
    void updateStudent(Student stoo);
    Boolean addStudent(Student stoo);
    int getStudentPrimaryKey(String id);
}
