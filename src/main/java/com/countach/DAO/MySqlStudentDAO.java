package com.countach.DAO;

import com.countach.Entity.Student;
import com.countach.Entity.StudentNotFoundException;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
@Qualifier("MySQL-Data")
public class MySqlStudentDAO implements Dao<Student>
{
    private DatabaseConnectionConfig DBCC;

    //Replace with own credentials
    private final String user = "******";
    private final String password = "*********";


    @Override
    public Student add(Student student) {

        Connection connect;
        PreparedStatement statement;

        try {
            connect = DBCC.getConnection();

            String query = "INSERT INTO Student(studentID, name, course) VALUES(?, ?, ?);";

            statement = connect.prepareStatement(query);
            statement.setString(1, student.getStudentID());
            statement.setString(2, student.getName());
            statement.setString(3, student.getCourse());
            statement.executeUpdate();

            System.out.println();

            statement.close();
            connect.commit();
            connect.close();
        }
        catch (Exception exp) {
            throw new RuntimeException("Failed to add Student!", exp);
        }

        return  student;
    }

    @Override
    public boolean delete(int ID) {

        Connection connect;
        PreparedStatement statement;

        try {

            connect = DBCC.getConnection();
            String query = "DELETE FROM Student WHERE ID = ?;";

            statement = connect.prepareStatement(query);
            statement.setString(1, String.valueOf(ID));
            statement.executeUpdate();

            statement.close();
            connect.commit();
            connect.close();

            return true;
        }
        catch (Exception exp) {
            throw new RuntimeException("Failed to delete Student!", exp);
        }
    }

    @Override
    public Student update(Student student) {

        String oldID = student.getStudentID().split("#")[1];
        String newID = student.getStudentID().split("#")[0];

        Connection connect;
        PreparedStatement statement;

        try {

            connect = DBCC.getConnection();

            /********************* Update course ********************/
            String query = "UPDATE Student SET course = ? WHERE studentID = ?;";

            statement = connect.prepareStatement(query);
            statement.setString(1, student.getCourse());
            statement.setString(2, oldID);
            statement.executeUpdate();

            /********************* Update name ********************/
            query = "UPDATE Student SET name = ? WHERE studentID = ?;";

            statement = connect.prepareStatement(query);
            statement.setString(1, student.getName());
            statement.setString(2, oldID);
            statement.executeUpdate();

            /********************* Update student_num ********************/
            query = "UPDATE Student SET studentID = ? WHERE name = ?;";

            statement = connect.prepareStatement(query);
            statement.setString(1, newID);
            statement.setString(2, student.getName());
            statement.executeUpdate();

            statement.close();
            connect.commit();
            connect.close();
        }
        catch (Exception exp) {
            throw new RuntimeException("Failed to update Student!", exp);
        }

        return student;
    }

    @Override
    public List<Student> getAll() {

        DBCC = new DatabaseConnectionConfig();
        DBCC.setUser(user);
        DBCC.setPass(password);

        ArrayList <Student> students = new ArrayList<>();

        Connection connection;
        Statement statement;
        ResultSet resultSet;

        try {
            String query = "SELECT * FROM Student;";

            connection = DBCC.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                students.add(new Student(resultSet.getString("student_num"),
                        resultSet.getString("name"),
                        resultSet.getString("course")));

                students.get(students.size() - 1).setRead(this.initTimestamp());
            }

            statement.close();
            connection.commit();
            connection.close();
        }
        catch(Exception exp) {
            throw new RuntimeException("Failed to get all students!", exp);
        }

        return students;
    }

    public Collection<Student> getStudentsByCourse(String course) {

        List<Student> students = this.getAll();

        if(students.size() > 0) {
            for(Student moon: students) {

                //Replace with ".match(Regex)" for sharper search.
                if(moon.getCourse().compareTo(course) != 0) {
                    students.remove(moon);
                }
            }
        }

        return students;
    }

    public Student getStudentByID(String ID) throws StudentNotFoundException {

        String stamp = this.initTimestamp();

        List<Student> students = this.getAll();

        if(students.size() > 0) {

            for (Student dent: students) {
                if(dent.getStudentID().compareTo(ID) == 0) {
                    System.out.println("Student found, returning!");
                    dent.setRead(stamp);
                    return dent;
                }
            }

        }

        System.out.println("Student NOT found, returning NULL!");
        throw new StudentNotFoundException("No student with ID '" + ID + "' exists.");
    }

    public int getStudentPrimaryKey(String ID) {

        Connection connect;
        Statement statement;
        ResultSet resultSet;

        try {

            connect = DBCC.getConnection();

            String query = "SELECT * FROM Student;";

            statement = connect.createStatement();
            resultSet = statement.executeQuery(query);

            while(resultSet.next()) {
                if(resultSet.getString("student_num").compareTo(ID) == 0) {
                    return (Integer.parseInt(resultSet.getString("ID")));
                }
            }

            statement.close();
            connect.commit();
            connect.close();
        }
        catch (Exception exp) {
            System.err.println(exp.getClass().getName() + ": " + exp.getMessage());
        }

        return -1;
    }

    private Boolean verifyDelete(String ID, ArrayList<Student> list){

        for(Student dent: list){
            if(dent.getStudentID().compareTo(ID) == 0){
                return false;
            }
        }

        return true;
    }

    private String initTimestamp(){

        String timeZone = (Integer.toString(LocalTime.now().getHour()));
        timeZone += (":" + Integer.toString(LocalTime.now().getMinute()));
        timeZone += (":" + Integer.toString(LocalTime.now().getSecond()));

        String stamp = "";
        LocalDate localDate = LocalDate.now();
        stamp += Integer.toString(localDate.getDayOfMonth());
        stamp += (" " + Month.of(localDate.getMonthValue()).name());
        stamp += (" " + Integer.toString(localDate.getYear()) + ", ");
        stamp += timeZone;

        return stamp;
    }
}
