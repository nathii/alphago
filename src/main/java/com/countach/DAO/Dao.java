package com.countach.DAO;

import java.util.List;

public interface Dao<T> {

    T add(T t);
    boolean delete(int id);
    T update(T t);
    List<T> getAll();
}
