package com.countach.DAO;

import com.countach.Entity.Student;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;


@Repository
@Qualifier("Mongo-Data")
public class MongoStudentDAO implements StudentDAO
{
    @Override
    public Collection<Student> getAllStudents()
    {
        return new ArrayList<Student>()
        {
            {
                add(new Student("D874521", "Martin", "BEng Chemical Engineering"));
            }
        };
    }

    @Override
    public Student getStudentByID(String ID)
    {
        return null;
    }

    @Override
    public Boolean deleteStudentByID(int ID, String studentID)
    {
        return null;
    }

    @Override
    public void updateStudent(Student stoo)
    {

    }

    @Override
    public Boolean addStudent(Student stoo)
    {
        return null;
    }

    @Override
    public int getStudentPrimaryKey(String id)
    {
        return 0;
    }

    @Override
    public Collection<Student> getStudentsByCourse(String co)
    {
        return null;
    }
}
