package com.countach.DAO;


import org.springframework.context.annotation.Configuration;

import java.sql.Connection;
import java.sql.DriverManager;

@Configuration
public class DatabaseConnectionConfig
{
    private String url;
    private String driver;
    private String pass;
    private String user;

    public DatabaseConnectionConfig() {

    }

    public DatabaseConnectionConfig(String url, String driver, String passw, String usr) {
        this.url = url;
        this.driver = driver;
        this.pass = passw;
        this.user = usr;
    }

    public void setUser(String usr) {
        this.user = usr;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getPass() {
        return pass;
    }

    public String getUser() {
        return user;
    }

    public Connection getConnection() {
        Connection connection = null;

        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, user, pass);
            connection.setAutoCommit(false);
        }
        catch(Exception exp) {
            exp.printStackTrace();
        }
        finally {
            return connection;
        }
    }
}
