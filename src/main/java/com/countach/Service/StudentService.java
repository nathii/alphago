package com.countach.Service;

import com.countach.DAO.PosgresqlStudentDao;
import com.countach.Entity.Student;
import com.countach.Entity.StudentNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * Created by countach on 2017/12/17.
 */

@Service
public class StudentService
{
    @Autowired
    private PosgresqlStudentDao dao;

    public StudentService() {

    }

    public Iterable<Student> getAllStudents() {
        return this.dao.findAll();
    }

    public Collection<Student> getStudentsByCourse(String cos) {
        return null;
    }

    public Optional<Student> getStudentByID(String ID) {

        Optional<Student> student = Optional.empty();
        try{

            Long id = Long.parseLong(ID);
            student = (this.dao.findById(id));
        }
        catch(StudentNotFoundException stu){
            System.out.println(stu.getMessage());
        }
        finally {

            return student;
        }
    }

    public void deleteStudentByID(String studentID) {
        Long id = Long.parseLong(studentID);
        this.dao.deleteById(id);
    }

    public void updateStudent(Student stoo) {
        //...
    }

    public Student addStudent(Student student) {
        return dao.save(student);
    }

    public int getStudentPrimaryKey(String id) {
        return Integer.MAX_VALUE;
    }
}
