package com.countach;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by countach on 2017/12/17.
 */

@SpringBootApplication
public class Initiator {

    //Project URL
    static String FB_BASE_URL= "FIREBASE PROJECT URL";

    public static void main(String[] args) {

        /***************** SpringBoot *****************/
        SpringApplication.run(Initiator.class, args);

        /**************** Firebase ****************/
        /*if(configureFirebase()){
            System.out.println("Firebase connection successfull!");
        }*/
    }

    /*private static Boolean configureFirebase(){

        Boolean status = false;

        try{

            ClassPathResource pathResource = new ClassPathResource("/firebase-authN.json");
            GoogleCredentials credentials = GoogleCredentials.fromStream(pathResource.getInputStream());

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(credentials)
                    .setDatabaseUrl(FB_BASE_URL)
                    .build();

            FirebaseApp.initializeApp(options);
            status = true;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            return status;
        }
    }*/
}
