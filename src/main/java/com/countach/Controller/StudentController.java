package com.countach.Controller;

import com.countach.Entity.Student;
import com.countach.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/students")
@CrossOrigin(origins = {"http://localhost:4200"})
@ResponseStatus(value = HttpStatus.OK, reason = "SUCCESS: Student controller reached!")
public class StudentController {

    @Autowired
    private StudentService studentService;

    /**
     * Queries and returns all stored database/data-source entries.
     *
     * @return a collection of Student objects (POJO)
     * @implNote Default-Route ==> http:/localhost:8080/students
     */
    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Student> getAllStudents() {
        return (studentService.getAllStudents());
    }


    /**
     * Queries/searches the database/data-source by ID, and retrives matching DB-Entry..
     *
     * @param ID student number (student_num) of student to be retrieved.
     * @return a Student object (POJO)
     * @implNote Route ==> localhost:8080/students/getStudentByID/{{student_num}}
     */
    @RequestMapping(path = "/getStudentByID/{ID}", method = RequestMethod.GET)
    public Optional<Student> getStudentByID(@PathVariable("ID") String ID) {
        return studentService.getStudentByID(ID);
    }


    /**
     * Queries/searches the database/data-source by ID, and retrives matching DB-Entry..
     *
     * @param course the course ID..
     * @return a Collection of Student objects (POJO)
     * @implNote Route ==> localhost:8080/students/getStudentsByCourse/{{courseID}}
     */
    @RequestMapping(path = "/getStudentsByCourse/{course}", method = RequestMethod.GET)
    public Collection<Student> getStudentsByCourse(@PathVariable("course") String course) {
        return studentService.getStudentsByCourse(course);
    }


    /**
     * Removes an entry from the database/data-source by ID.
     *
     * @param ID student number (student_num) of student to be deleted.
     * @return a message detailing successful/failed deletion
     * @implNote Route ==> localhost:8080/students/deleteStudentByID/{{student_num}}
     */
    @RequestMapping(path = "/deleteStudentByID/{ID}", method = RequestMethod.DELETE)
    public void deleteStudentByID(@PathVariable("ID") String ID) {
        studentService.deleteStudentByID(ID);
    }

    /**
     * Updates an existing entry in the database/data-source
     *
     * @param stoo Object to be added
     * @return a message detailing successful/failed update
     * @implNote Rest-Client: Use Text input in Request Body and provide Student Object as JSON.
     * @implNote Add "Content-Type: application/json" header in the Headers tab (Rest-Client).
     * @implNote Route ==> localhost:8080/students/updateStudent
     */
    @RequestMapping(path = "/updateStudent", method = RequestMethod.POST, consumes = "application/json")
    public String updateStudent(@RequestBody Student stoo) {

        if (stoo != null) {
            studentService.updateStudent(stoo);
            return "Student updated!";
        }

        return "Student update failed!";
    }


    /**
     * Adds a student to the database/data-source, POJO is translated to a database entry.
     *
     * @param student Object to be added
     * @return a message detailing successful/failed addition
     * @implNote Rest-Client: Use Text input in Request Body and provide Student Object as JSON.
     * @implNote Add "Content-Type: application/json" header in the Headers tab (Rest-Client).
     * @implNote Route ==> localhost:8080/students/addStudent
     */
    @RequestMapping(path = "/addStudent", method = RequestMethod.POST, consumes = "application/json")
    public Student addStudent(@RequestBody Student student) {

        return studentService.addStudent(student);
    }
}
