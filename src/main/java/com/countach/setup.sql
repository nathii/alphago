create table if not exists Student(
	ID int(10) not null AUTO_INCREMENT,
	student_number VARCHAR(8) not null,
	name varchar(50) not null,
	course varchar(50) not null,
	primary key(ID)
);
