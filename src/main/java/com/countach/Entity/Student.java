package com.countach.Entity;

/********** This is our POJO, Model or Bean ************/

public class Student
{
    private String studentID;
    private String name;
    private String course;
    private String read;

    public String getRead()
    {
        return read;
    }

    public void setRead(String read)
    {
        this.read = read;
    }


    public Student(){}

    public Student(String ID, String name, String course)
    {
        this.studentID = ID;
        this.name = name;
        this.course = course;
    }

    public String getStudentID()
    {
        return studentID;
    }

    public String getName()
    {
        return name;
    }

    public String getCourse()
    {
        return course;
    }

    public void setstudentID(String ID)
    {
        this.studentID = ID;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setCourse(String course)
    {
        this.course = course;
    }

    @Override
    public String toString()
    {
        return "Student: [" +
                "student_num = '" + studentID + '\'' +
                ", name = '" + name + '\'' +
                ", course = '" + course + '\'' +
                ']';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Student student = (Student) o;

        if (!studentID.equals(student.studentID)) return false;
        if (!name.equals(student.name)) return false;
        return course.equals(student.course);
    }

    @Override
    public int hashCode()
    {
        int result = studentID.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + course.hashCode();
        return result;
    }
}
