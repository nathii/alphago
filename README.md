# *Alpha - GO*
---

#### An Angular 4 CRUD application for custom types/objects. Below is the application development stack:

 - **[Angular 4](https://angular.io/), [Typescript](https://www.typescriptlang.org/), [Bootstrap](https://valor-software.com/ngx-bootstrap/old/2.0.4/#/)**: *Front-end development*
 - **Java + [SpringBoot](https://spring.io/projects/spring-boot)**: *Implementation of REST services and interfacing with the data-layer*
 - **[MySQL](https://www.mysql.com/), [MongoDB](https://docs.mongodb.com/) and [Firebase](https://firebase.google.com/)**: *Multiple implementations of the data-layer. SpringBoot allows the quick change and selection of which implementation of the data-layer/DB to use via the use of the [@Qualifier](https://spring.io/blog/2014/11/04/a-quality-qualifier) annotation.*
 
 ---
 
#### TODO: Architectural changes:

```
1. Re-implement the data layer (Hibernate)
2. Migrate to Angular 6
3. Seperate views into relevant components
4. Scale up to Spring-boot 2.*.* RELEASE
5. Dockerize databases (Mysql, Mongo)
6. Add CI/CD with Jenkins
7. Monolith --> Microserves
```
---

#### Execution:

```
$ git clone https://github.com/Nathi360/AlphaGO.git
$ cd AlphaGO-master
$ make && make run        # Run the Spring-Boot server (backend)

$ cd AlphaGO-master/src/main/Client/Spring-CRUD/
$ ng build && ng serve    # Run the Angular application (in a new terminal session)

```
 
 ---
 
Preview:

![Imgur](https://i.imgur.com/0SzDQRJ.png)


 